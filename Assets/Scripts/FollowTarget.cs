﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public Transform TargetToFollow;

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.position = new Vector3(TargetToFollow.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
    }
}
