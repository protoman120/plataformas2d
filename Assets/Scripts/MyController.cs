﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]

public class MyController : MonoBehaviour
{

    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;
    public float JumpForce = 1000f;
    private bool IsOnGround = false;
    private bool attack = false;
    public int transformX = 0;
    public GameObject Graphics;
    private float HorizontalDirection;
    [SerializeField] private Animator animator;

    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        move = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);

        if (IsOnGround == true && (Input.GetButtonDown("Jump"))){
            rb2d.AddForce(transform.up * JumpForce);
        }

        if (Math.Abs(rb2d.velocity.x) > 0)
        {
        }
    }

    private void Update()
    {
        attack = Input.GetButton("Fire1");

        HorizontalDirection = Graphics.transform.localScale.x;
        if (move > 0 && HorizontalDirection < 0)
        {
            Graphics.transform.localScale = new Vector3(1, 1, 1);

        }else if (move < 0 && HorizontalDirection > 0)
        {
            Graphics.transform.localScale = new Vector3(-1, 1, 1);
        }

        if (attack = true)
        {
            move = 0f;
            IsOnGround = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Ground"){
            IsOnGround = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        IsOnGround = false;
    }


}
